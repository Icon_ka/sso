package config

import (
    "os"
    "time"
    "github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
    Env        string        `yaml:"env" env-default:"local"`
    TokenTTL   time.Duration `yaml:"token_ttl" env-required:"true"`
    DB         `yaml:"db"`
    GRPCConfig `yaml:"grpc"`
}

type DB struct {
    DbUser     string `yaml:"db_user"`
    DbPort     string `yaml:"db_port"`
    DbName     string `yaml:"db_name"`
    DbHost     string `yaml:"db_host"`
    DbPassword string `yaml:"db_password"`
}

type GRPCConfig struct {
    Port    string `yaml:"port"`
    Timeout string `yaml:"timeout"`
}

func MustLoad() *Config {
    configPath := os.Getenv("CFG_PATH")
    if configPath == "" {
        panic("cfg path is not set")
    }

    if _, err := os.Stat(configPath); os.IsNotExist(err) {
        panic("cfg path does not exist: " + configPath)
    }

    var cfg Config

    err := cleanenv.ReadConfig(configPath, &cfg)
    if err != nil {
        panic("cannot read config: " + err.Error())
    }

    return &cfg
}
