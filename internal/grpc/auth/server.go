package auth

import (
    "context"
    ssov1 "gitlab.com/Icon_ka/proto/prot/gen/go/sso"
    "google.golang.org/grpc"
)

type serverAPI struct {
    ssov1.UnimplementedAuthServer
}

func Register(gRPC *grpc.Server) {
    ssov1.RegisterAuthServer(gRPC, &serverAPI{})
}

func (s *serverAPI) Login(ctxt context.Context, request *ssov1.LoginRequest) (*ssov1.LoginResponse, error) {
    panic("DA")
}

func (s *serverAPI) Register(ctxt context.Context, request *ssov1.RegisterRequest) (*ssov1.RegisterResponse, error) {
    panic("PIZDA")
}

func (s *serverAPI) IsAdmin(ctx context.Context, request *ssov1.IsAdminRequest) (*ssov1.IsAdminResponse, error) {
    panic("net")
}
