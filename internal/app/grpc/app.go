package grpc

import (
    "golang.org/x/exp/slog"
    "google.golang.org/grpc"
    authgrpc "gitlab.com/Icon_ka/sso/internal/grpc/auth"
)

type App struct {
    log        *slog.Logger
    gRPCServer *grpc.Server
    port       string
}

func New(log *slog.Logger, port string) *App {
    gRPCServer := grpc.NewServer()

    authgrpc.Register(gRPCServer)

    return &App{
        log:        log,
        gRPCServer: gRPCServer,
        port:       port,
    }
}
