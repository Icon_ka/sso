package main

import (
    "gitlab.com/Icon_ka/sso/internal/config"
    "github.com/joho/godotenv"
    "log"
    "os"
    "golang.org/x/exp/slog"
)

func main() {
    err := godotenv.Load("../../.env")
    if err != nil {
        log.Fatalf("error open env: %v", err)
    }
    cfg := config.MustLoad()

    log := setupLogger(cfg.Env)

    log.Info(
        "starting app",
    )
}

func setupLogger(env string) *slog.Logger {
    var log *slog.Logger
    switch env {
    case "local":
        log = slog.New(
            slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}),
        )
    }

    return log
}
